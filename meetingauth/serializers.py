from rest_framework import serializers
from .models import MeetingUser

class MeetingUserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)  # Define a write-only field for password

    class Meta:
        model = MeetingUser
        fields = ['id', 'username', 'email', 'password', 'first_name', 'last_name', 'country_code', 'language_code', 'promocode', 'terms_agreement']
        extra_kwargs = {
            'password': {'write_only': True},  # Ensure password is write-only
        }

    def create(self, validated_data):
        password = validated_data.pop('password')  # Remove password from validated_data
        user = MeetingUser.objects.create(**validated_data)
        user.set_password(password)  # Use set_password to hash the password
        user.save()
        return user