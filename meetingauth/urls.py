from django.urls import path, include
from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from .views import MeetingUserViewSet

# Create a router for handling MeetingUserViewSet
router = DefaultRouter()
router.register(r'meetingusers', MeetingUserViewSet, basename='meetinguser')

urlpatterns = [
    # Include the API endpoints from the router
    path('', include(router.urls)),
    
    # Token-based authentication endpoints
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]
