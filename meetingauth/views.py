from rest_framework import viewsets
from .models import MeetingUser
from .serializers import MeetingUserSerializer

class MeetingUserViewSet(viewsets.ModelViewSet):
    queryset = MeetingUser.objects.all()
    serializer_class = MeetingUserSerializer

    def perform_create(self, serializer):
        # Ensure password encryption when creating a new user
        serializer.save()