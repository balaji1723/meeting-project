from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import MeetingUser

class CustomUserAdmin(UserAdmin):
    model = MeetingUser
    list_display = ('username', 'email', 'first_name', 'last_name', 'country_code', 'language_code', 'promocode', 'is_active', 'is_staff', 'is_superuser', 'date_joined')
    search_fields = ('username', 'email', 'first_name', 'last_name')
    readonly_fields = ('date_joined',)

    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        ('Personal Info', {'fields': ('first_name', 'last_name', 'email',)}),
        ('Permissions', {'fields': ('is_active', 'is_staff', 'is_superuser', 'country_code', 'language_code')}),
        ('Important dates', {'fields': ('last_login', 'date_joined')}),
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'email', 'password1', 'password2', 'first_name', 'last_name', 'country_code', 'language_code', 'promocode', 'is_active', 'is_staff', 'is_superuser'),

        }),
    )

    # Remove filter_horizontal and list_filter
    filter_horizontal = ()
    list_filter = ('is_staff', 'is_superuser', 'is_active')

# Register the custom UserAdmin with the User model
admin.site.register(MeetingUser, CustomUserAdmin)
